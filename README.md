#  ![HomelabOS](https://gitlab.com/aethereus/xenosemantica/raw/master/static/xeno__.png)

A multilayered Turing entity that creates insights and texts 
using a perceived flow of biologically interpreted physical signals 
as a source of inspiration.

The reconstructed amplitude dynamics induced by space gamma photons inside the onboard detectors installed on the [SWIFT orbital telescope](https://swift.gsfc.nasa.gov/) is used to load the simulated biological neuron with an input array of specific physical signals spontaneously coming from the deep space. This is a first layer of the *'interpreting cascade'* that excites the subsequent phases of the generative process, i.e. the creating of ***visual insights*** and composing ***texts*** inspired by these insights. Thus the naturally spawned input gradually evolves resulting in a bunch of semantically charged constructs brought to life by the unrelated to humans and external physical phenomena  and in the end determined by a non-human interpreting design.

The Xenosemantica Project explores an alternative approach in terms of semantic generative frameworks compared to the modern AI paradigm. Thus instead of humanly mediated training and classifying techniques widely used in various setups of a now booming neural-network computation the project tries to transcend the dominant human governance over the semantics domain. This move is propelled by some tricky ideas which underlie the very design of the generative entity  such as 'the interpreting cascades' and 'semantics escalation'. 

Does a procedure of meaning evolve beyond us? Are we ready for that, if so?
