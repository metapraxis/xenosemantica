from telethon import TelegramClient

# Telethon wrapper for messages exchange via private channel

class ChannelRoutine():

    api_id = 4293615
    api_hash = 'bc47c096c086ae3e9b5084287d1798ad'
    bot_token = '1702139550:AAGzgwcTrgRqSSQhiWNNprR50K95tKyLkhY'
    channel_hash = 'X9EO7FRZMlU4OWUy'
    chat_id = -1001461859825
    time2sleep = 30

    def __init__(self):

        self.client = TelegramClient('anon', ChannelRoutine.api_id, ChannelRoutine.api_hash)
        # self.bot = TelegramClient('bot', ChannelRoutine.api_id, ChannelRoutine.api_hash).start(bot_token=ChannelRoutine.bot_token)

    def send2channel(self, raw_verse):

        async def main():
            message = await self.client.send_message(ChannelRoutine.chat_id, raw_verse)
            print(message.id)
            return message.id

        with self.client:
            message_id = self.client.loop.run_until_complete(main())

        return message_id


    def __readreply__(self, message_id):

        async def main():
            async for message in self.client.iter_messages(ChannelRoutine.chat_id, reply_to=message_id):
                print(message.id, message.text)
                return message.text

        with self.client:
            reply_verse = self.client.loop.run_until_complete(main())

        return reply_verse

    def check_reply(self, message_id):
        while True:
            reply = self.__readreply__(self, message_id)
            if reply is None:
                print('waiting ',TGChannelRoutine.time2sleep,'s')
                time.sleep(TGChannelRoutine.time2sleep)
                continue
            else:
                print('reply is... ', reply )
                return reply

