#!/home/ivan/PycharmProjects/higher_design/venv/bin/python

import bs4 as bs
import requests
import json
from six.moves import urllib
import os
import time
from socket import error as SocketError

def get_source_from_swift(swift_url,time2sleep):
    while True:
        try:
            source = requests.get(swift_url)
            break
        except requests.exceptions.RequestException:
            time.sleep(time2sleep)
    return source

def get_url_paths(url_seq, ext, params={}):


    response = requests.get(url_seq, params=params)

    if response.ok:
        response_text = response.text
    else:
        return response.raise_for_status() # handling an error if virtual url fails

    soup = bs.BeautifulSoup(response_text, 'html.parser')
    parent = [url_seq + node.get('href') for node in soup.find_all('a') if node.get('href').endswith(ext)]

    return parent

def get_fits_file(path2largest, path2download):
    while True:
        try:
            urllib.request.urlretrieve(path2largest, path2download)
            return True
        except SocketError:
            time.sleep(60)


def get_fits_list(swift_url,time2sleep,file_ext):

    '''
    Scans SWIFT's Mission QuickLook Data page for a new GRB entry
    If a new GRB pops up it tries to download the largest events file associated with it (IF there is any) and
    returns a local path to a new events file
    Returns None if no new GRB or no events file is found
    '''
    quicklook_table = []

    source = get_source_from_swift(swift_url,time2sleep)

    soup = bs.BeautifulSoup(source.content, 'lxml')
    table = soup.table
    table_rows = table.find_all('tr')

    try:
        with open('{}/data_run/grbs_processed.json'.format(os.getcwd()), 'r') as grbs_processed:
            grbs_base = json.load(grbs_processed)  # returns a list
    except:  # if starts with an empty list of GRBs
        grbs_base = []


    for tr in table_rows:
        td = tr.find_all('td')
        row = [i.text for i in td]
        quicklook_table.append(row)

    for i in quicklook_table[1:]:  # NB: 1st row in quicklook_table is empty!

        if any(x in i[2] for x in ['GRB', 'Burst']):  # "GRB" or 'Burst' mark desired sequence in a quicklook_table

            if '{}:{}'.format(i[2], i[0][:8]) in grbs_base:
                pass
            else:
                try:
                    seq_vers = str(i[0]) + '.' + str(i[1]) # composing a virtual sequence
                    print(seq_vers)

                    url_seq = 'https://swift.gsfc.nasa.gov/data/swift/sw{}/bat/event/'.format(seq_vers)    # composing a virtual path

                    files_paths = get_url_paths(url_seq, ext=file_ext) # checking a virtual path to handle an http error if it fails
                    files_length = {}

                    for k in files_paths:
                        response = requests.head(k)
                        files_length[k] = (int(response.headers['Content-Length']))

                    path2largest = max(files_length, key=lambda i: files_length[i])

                    path2download = '{}/astro_data/{}_{}@{}'.format(os.getcwd(), seq_vers, i[2], i[3])

                    if get_fits_file(path2largest,path2download):
                        pass
                    else:
                        break

                    grbs_base.append('{}:{}'.format(i[2], i[0][:8])) # e.g. ["GRB Discovered Elsewhere:00020895:2019-06-13T10:29:00",...]

                    print('New GRB is {}/astro_data/{}_{}'.format(os.getcwd(), seq_vers, i[2]))

                    with open('{}/data_run/grbs_processed.json'.format(os.getcwd()), 'w') as grbs_processed:
                        json.dump(grbs_base, grbs_processed)

                    return '{}/astro_data/{}_{}@{}'.format(os.getcwd(), seq_vers, i[2], i[3])

                except requests.exceptions.HTTPError:
                    pass

    return None
