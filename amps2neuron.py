from neuron import h
from data_run import DataRun

datarun = DataRun()

h.load_file("stdgui.hoc")  # doesn't bring up the NEURON Main Menu toolbar

res = 10e-6        # resistance to convert V to amp

soma = h.Section(name='soma')
dend = h.Section(name='dend')

dend.connect(soma(1))

soma.L = soma.diam = 12.6157
dend.L = 200
dend.diam = 1

for sec in h.allsec():
    sec.Ra = 100    # Axial resistance in Ohm * cm
    sec.cm = 1      # Membrane capacitance in micro Farads / cm^2

# Insert active Hodgkin-Huxley current in the soma
soma.insert('hh')
for seg in soma:
    seg.hh.gnabar = 0.12  # Sodium conductance in S/cm2
    seg.hh.gkbar = 0.036  # Potassium conductance in S/cm2
    seg.hh.gl = 0.0003    # Leak conductance in S/cm2
    seg.hh.el = -54.3     # Reversal potential in mV

# Insert passive current in the dendrite
dend.insert('pas')
for seg in dend:
    seg.pas.g = 0.001  # Passive conductance in S/cm2
    seg.pas.e = -65    # Leak reversal potential mV

stim = h.IClamp(dend(1))

stim.delay = 5
stim.dur = 1

t_vec = h.Vector()        # Time stamp vector
v_vec = h.Vector()        # Membrane potential vector
v_vec.record(soma(0.5)._ref_v)
t_vec.record(h._ref_t)
simdur = 15.0

# pyplot.figure(figsize=(8,4))

def get_mem_voltages(det_amps, time_of_event, v_vec=v_vec):
    m_voltages = []
    for i in det_amps[30:31], det_amps[200:201]: #,indet_x[180:182]: ### >>> produces list of arrays
        # print(i)
        for k in i:
            if k < 0:
                pass
            else:
                # print(k)
                stim.amp = k / res              # 'if': in case the detected voltage is
                h.tstop = simdur  # type:#float!!! # calibrative and/or of negative value
                h.run()
                m_voltages.append(v_vec.as_numpy()*10e-4)


    mem_volts = {}
    mem_volts['flag'] = True
    mem_volts['time'] = time_of_event
    mem_volts['m_voltages'] = m_voltages

    datarun.store_memvoltages(mem_volts)

    return mem_volts


    # print(mem_volts['m_voltages'][0][:10])
    # print('New GRBs processed, now waiting 60 mins for the next scan')

