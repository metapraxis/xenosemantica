# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
# Copyright 2020 (new and modified components) XenoSemantica. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================

"""Simple image classification module with Inception.

Modified image classification code with Inception trained on ImageNet 2012 Challenge data
set.

This program creates a graph from a saved GraphDef protocol buffer,
and runs inference on an input image(s). It outputs human readable
strings of the predictions along with their probabilities.

See: https://tensorflow.org/tutorials/image_recognition/
"""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os.path
import re
import numpy as np
from multiprocessing import Pool

# Set TF logging to 0
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
import tensorflow as tf

# Set TF verbosity to 0
tf.compat.v1.logging.set_verbosity(tf.compat.v1.logging.ERROR)

paths = []

# paths to Inception models labels graphs
label = '{}/t_flow_models_and_graphs/imagenet_2012_challenge_label_map_proto.pbtxt'.format(os.getcwd())
uid = '{}/t_flow_models_and_graphs/imagenet_synset_to_human_label_map.txt'.format(os.getcwd())
def_graph = '{}/t_flow_models_and_graphs/classify_image_graph_def.pb'.format(os.getcwd())

# a number of the finally produced predictions for the each insight image
n_predict = 30

class NodeLookup(object):
  """Converts integer node ID's to human readable labels."""
  def __init__(self, label_lookup_path=label, uid_lookup_path=uid):
    self.node_lookup = self.load(label_lookup_path, uid_lookup_path)

  def load(self, label_lookup_path, uid_lookup_path):
    """Loads a human readable English name for each softmax node.

    Args:
      label_lookup_path: string UID to integer node ID.
      uid_lookup_path: string UID to human-readable string.

    Returns:
      dict from integer node ID to human-readable string.
    """

    # Loads mapping from string UID to human-readable string
    proto_as_ascii_lines = tf.io.gfile.GFile(uid_lookup_path).readlines()
    uid_to_human = {}
    p = re.compile(r'[n\d]*[ \S,]*')
    for line in proto_as_ascii_lines:
      parsed_items = p.findall(line)
      uid = parsed_items[0]
      human_string = parsed_items[2]
      uid_to_human[uid] = human_string

    # Loads mapping from string UID to integer node ID.
    node_id_to_uid = {}
    proto_as_ascii = tf.io.gfile.GFile(label_lookup_path).readlines()
    for line in proto_as_ascii:
      if line.startswith('  target_class:'):
        target_class = int(line.split(': ')[1])
      if line.startswith('  target_class_string:'):
        target_class_string = line.split(': ')[1]
        node_id_to_uid[target_class] = target_class_string[1:-2]

    # Loads the final mapping of integer node ID to human-readable string
    node_id_to_name = {}
    for key, val in node_id_to_uid.items():
      if val not in uid_to_human:
        tf.logging.fatal('Failed to locate: %s', val)
      name = uid_to_human[val]
      node_id_to_name[key] = name

    return node_id_to_name

  def id_to_string(self, node_id):
    if node_id not in self.node_lookup:
      return ''
    return self.node_lookup[node_id]


def create_graph():
  """Creates a graph from saved GraphDef file and returns a saver."""
  # Creates graph from saved graph_def.pb.

  with tf.io.gfile.GFile(def_graph, 'rb') as f:
    graph_def = tf.compat.v1.GraphDef()
    graph_def.ParseFromString(f.read())
    _ = tf.import_graph_def(graph_def, name='')


def run_inference_on_image(image):
  """Runs inference on an image.

  Args:
    image: Image file name.

  Returns:
    Nothing
  """
  predicts_all = []

  image_data = tf.io.gfile.GFile(image, 'rb').read()

  # Creates graph from saved GraphDef.
  create_graph()

  with tf.compat.v1.Session() as sess:
    # Some useful tensors:
    # 'softmax:0': A tensor containing the normalized prediction across
    #   1000 labels.
    # 'pool_3:0': A tensor containing the next-to-last layer containing 2048
    #   float description of the image.
    # 'DecodeJpeg/contents:0': A tensor containing a string providing JPEG
    #   encoding of the image.
    # Runs the softmax tensor by feeding the image_data as input to the graph.

    softmax_tensor = sess.graph.get_tensor_by_name('softmax:0')
    predictions = sess.run(softmax_tensor,
                           {'DecodeJpeg/contents:0': image_data})
    predictions = np.squeeze(predictions)

    # Creates node ID --> English string lookup.
    node_lookup = NodeLookup()

    top_k = predictions.argsort()[-n_predict:][::-1]
    for node_id in top_k:
      human_string = node_lookup.id_to_string(node_id)
      # score = predictions[node_id]
      predicts_all.append(human_string)
      #print('%s (score = %.5f)' % (human_string, score))

  return predicts_all




def run_tf(paths):
    # tf.reset_default_graph() # prevents raising the ValueError("GraphDef cannot be larger than 2GB.")
    # predicts_final_list = []
    # for f in os.listdir(path2insights):
    # print(paths)
    pool = Pool(6)
    predicts_final_list = pool.map(run_inference_on_image, [image for image in paths])
    pool.close()
    return predicts_final_list

def get_predicts(paths):
  '''
  gets list of lists of predicts (of different score) generated by run_tf, e.g. [['rule, web site'], ['handkerchief, oscilloscope'],...]
  returns a set of predicts (no duplicates), e.g. {'cover', 'cabinet', 'CRO', 'book', 'handkerchief'...}
  '''
  predicts = run_tf(paths)
  predicts_split = []
  for i in range(len(predicts)): # split predicts, e.g. [['rule, web site'],...] to [['rule'], ['web site'],...]
      for k in predicts[i]:
          predicts_split.append(k.split(', '))
  split_deep = []                       # split predicts deeper, e.g [['rule'], ['web site'],...]
  for i in range(len(predicts_split)):  # to [['rule'], ['web'], ['site'],..]
      for k in predicts_split[i]:
          split_deep.append(k.split(' '))
  predicts_set = set()           # filter the duplicated predicts with set()
  for i in range(len(split_deep)):
      for k in split_deep[i]:
          predicts_set.add(k)
  return predicts_set
