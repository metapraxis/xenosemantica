from astropy.table import Table

def kev2amp(path2events):
    """
    takes: detected photon energies (energ = [],ints,keV) from BAT fits file
    returns: a list (u = []) of reconstructed charge amplitudes (V) produced in a single
    detector (or detectors)

    el = 1.6e-19 is electron's charge, C
    cap = detector'S capacitance, e.g. CdZnTe, F
    ev1000 = 1e3, 1 keV = 1000 eV
    nrg = float, electron's transfer energy, eV (depends on type of semiconductor)
    evts = int, number of events in fits file (int)

    u = [], list of charge amplitudes (V), U = Q/C, where Q = (E/et)*e,
    where Q is a charge produced inside detector (eV), C - detector's capacitance,
    E - detected photon energy, et - electron's transfer energy (eV), e - electron's charge
    """
    diap = 100000
    step = 500

    el = 1.6e-19 # electron's charge, C
    cap = 1e-11  # Detector CdZnTe capacitance (F)
    nrg = 4.5  # Electron's transfer energy, eV (depends on type of semiconductor,e.g. CdZnTe)

    ev1000 = 1e3

    events = Table.read(path2events, hdu=1)
    evts = events[:int(1e6)]  # number of events from a fits file (int)

    energ = []
    u = []

    for i in evts[diap: diap+step]:
            energ.append(i['ENERGY'])
    for i in energ:
        u.append(round(((i * ev1000 / nrg) * el) / cap, 7))
    return u

# with open("data_run/detected_amps.json", 'w') as amps:
#     json.dump(kev2amp(diap=d1), amps)





