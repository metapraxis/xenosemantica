from swift_data import SwiftData
from data_run import DataRun
from x_automation import TGChannelRoutine, FbAutomaton
import kev2amp
import amps2neuron
import multiprocMakeInsights
import class_nsights
import verse_from_predicts
import time
import os
import traceback
import random

# TODO make post etc from processed event in json

path2save_insights = 'testing_make_insights'
time2sleep = 60*30     # time the next try runs if requests error occurs
zeroruns_count = 48  # counts of times to start the next insights generating cycle if there is no new data from SWIFT,
                    # e.g. if time2sleep = 60 in 60*10 waiting cycle 144 zeroruns counts = ((60*10)*144)secs = 24hrs
swift = SwiftData()
datarun = DataRun()

def assembly_run():
    count = 0
    while True:
        try:
            path2events = swift.get_fits_list() # returns e.g. '~/astro_data/00907950002.003_GRB190611a@2019-06-13T12:09:34'
            # path2events = 'astro_data/2021-11-27T00:32:36:M31N2008-12a:00014920013:002' # testing mode

            if path2events or count == zeroruns_count:

                if count == zeroruns_count:
                    print('if count == zeroruns_count:')
                    path2events = 'astro_data/' + (''.join(os.listdir('astro_data')[-1:]))

                datarun.purge_astrodata(path2events)  # delete all old astro files except the freshest one

                print('count is',count)

                processed_sequence = datarun.read_astro_objects()['all_processed']

                event_id = ''.join(processed_sequence[-1:]) #  event_id = '01048783000'
                burst_name = datarun.read_astro_objects()[event_id]['name']
                burst_date = datarun.read_astro_objects()[event_id]['date']

                detected_amps = kev2amp.kev2amp(path2events)

                amps2neuron.get_mem_voltages(detected_amps)

                path2save = '{0}/{1}:{2}:{3}___{4}'.format(path2save_insights,
                                                           event_id,
                                                           burst_name,
                                                           burst_date,
                                                           str(random.randint(1,999))) # we need random addition to directory name in case of zero runs or testing mode

                os.mkdir(path2save)

                path2insights = multiprocMakeInsights.makeImage(path2save, event_id)  # returns True or False, makeImage may be unstable

                if path2insights:

                    paths = []      # make images paths list to get predicts
                    for f in os.listdir(path2save):
                        paths.append(path2save + '/' + f)

                    get_predicts = list(class_nsights.get_predicts(paths))  # convert a set of unique predicts to list

                    verses = verse_from_predicts.generate_verses(get_predicts)

                    datarun.add_attributes_2astroobject(event_id,
                                                        verse = verses[0],
                                                        triggers = verses[1],
                                                        zero_predicts = verses[2])

                    count = 0

                    event_id_4posting = f"\n\nTime: {burst_date}\nSWIFT event: {burst_name}"

                    tg_routine = TGChannelRoutine()

                    message_tg = "\n".join(map(str,verses[0])) + event_id_4posting + event_id
                    message_id = tg_routine.send2_xeno_s(message_tg) # .send2channel returns ID of the TG message

                    try_verse = 0

                    while True:
                        reply = tg_routine.readreply(message_id)

                        if reply == 'next':
                            datarun.add_parsed_sequence(event_id)
                            datarun.add_last_date(burst_date)
                            break

                        if reply is None:
                            print('waiting ', TGChannelRoutine.time2sleep, 's')
                            time.sleep(TGChannelRoutine.time2sleep)
                            continue

                        if reply == 'wait':
                            print('waiting 60 min')
                            time.sleep(time2sleep * 2)
                            continue

                        if reply == 'try':
                            try_verse += 1
                            verses = verse_from_predicts.generate_verses(get_predicts)
                            datarun.add_attributes_2astroobject(event_id,
                                                                try_verse='try {0}: {1}'.format(try_verse, verses[0]),
                                                                try_triggers='try {0}: {1}'.format(try_verse, verses[1]),
                                                                try_zero_predicts='try {0}: {1}'.format(try_verse, verses[2]))
                            message_tg = "\n".join(map(str, verses[0])) + event_id_4posting + '\ntry verse {0}'.format(try_verse)
                            message_id = tg_routine.send2_xeno_s(message_tg)  # .send2channel returns ID of the TG message
                            continue

                        if reply:
                            insights_hashes = []
                            for i in paths:
                                hash_ = datarun.__make_hashes__(i)
                                insights_hashes.append(hash_)
                                new_name = path2save + '/' + hash_+ '.png'
                                os.rename(i, new_name)

                            paths = []
                            for f in os.listdir(path2save):
                                paths.append(path2save + '/' + f)

                            verse_hash = datarun.__make_str_hash__(reply)

                            datarun.add_last_date(burst_date)
                            datarun.add_attributes_2astroobject(event_id, reply = reply, insights_hashes = insights_hashes, verse_sha256 = verse_hash)
                            datarun.insert_verse_in_dir(path2save, reply, event_id, burst_name, burst_date)
                            path2archive = datarun.make_nft_archive(reply,
                                                                    paths[0],
                                                                    path2save,
                                                                    event_id,
                                                                    burst_name,
                                                                    burst_date,
                                                                    insights_hashes,
                                                                    verse_hash)
                            datarun.make_bc_timestamp(path2archive)

                            demo_ipfs_cid = datarun.read_astro_objects()[event_id].get('demo_ipfs_cid')

                            tg_routine.send2_main_channel(path2insight=paths[0],
                                                          caption='{0}{1}\nipfs://{2}'.format(reply, event_id_4posting, demo_ipfs_cid))

                            fb_posting = FbAutomaton(browser='Firefox')
                            time.sleep(3)
                            check_fb = fb_posting.fb_post(post=reply + event_id_4posting + '\n' + 'IPFS: '+ demo_ipfs_cid + \
                                                               '\n' + 'Telegram: https://t.me/xenosemantica', img_path=paths[0])
                            # check_fb = False # testing mode
                            if check_fb is True:
                                tg_routine.send2_xeno_s('FB posting succeded: {0}'.format(event_id))
                                break
                            else:
                                tg_routine.send2_xeno_s('FB posting failed: {0}'.format(event_id))
                                break
                else:
                    tg_routine = TGChannelRoutine()
                    tg_routine.send2_xeno_s('No insights: {0}'.format(event_id))
                    continue
            else:
                count += 1
                print('#', count, 'waiting for new events from SWIFT...', time2sleep, 's')
                time.sleep(time2sleep)

        except Exception:
            with open('log', 'w+') as f:
                traceback.print_exc(file=f)
            print('something went wrong, see log, waiting', time2sleep, 's')
            time.sleep(time2sleep)
            continue

if __name__ == '__main__':
        assembly_run()