import json
import pickle
import os
from zipfile import ZipFile
from os.path import basename
import shutil
import hashlib
import subprocess
import make_pdf
# from nft_processing import All_Pinata
from astropy.table import Table


# pinata = All_Pinata()

class DataRun:
    """The methods to store/purge/read/zip/hash/timestamp operational data"""
    def __init__(self):
        self.path2memvoltages = 'data_run/membrane_voltages_pckl'
        self.path2astro_data = 'astro_data'
        self.path2processed_objects = 'data_run/processed_m.json'
        self.path2insights = 'testing_make_insights'
        self.path2data_run = 'data_run'
        self.path2json2datable = 'data_run/json4datatable.json'

    def store_memvoltages(self, data_in: dict):
        """Just store resulting dict in a pickle file """
        with open(self.path2memvoltages, 'wb') as spikevolts:
            pickle.dump(data_in, spikevolts)

    def read_memvoltages(self):
        with open(self.path2memvoltages, 'rb') as volts:
            mem_voltages = pickle.load(volts)
        return mem_voltages

    def store_astro_object(self, processed_sequence, **kwargs):
        """kwargs: name, date, sequence_vers"""
        with open(self.path2processed_objects, 'r') as f:
            processed = json.load(f)
        processed[processed_sequence] = {}
        processed['all_processed'].append(processed_sequence)
        for key, value in kwargs.items():
            processed[processed_sequence][key] = value
        with open(self.path2processed_objects, 'w') as f:
            json.dump(processed, f)

    def add_attributes_2astroobject(self, processed_sequence, **kwargs):
        """Gets generated data and inserts it into nested dict"""
        with open(self.path2processed_objects, 'r') as f:
            processed = json.load(f)
        for key, value in kwargs.items():
            processed[processed_sequence][key] = value
        with open(self.path2processed_objects, 'w') as f:
            json.dump(processed, f)

    def add_parsed_sequence(self, sequence):
        """Adds parsed sequence to the resulting dict
        """
        with open(self.path2processed_objects, 'r') as f:
            processed = json.load(f)
        processed['all_parsed'].append(sequence)  # set is not JSON serializable
        with open(self.path2processed_objects, 'w') as f:
            json.dump(processed, f)

    def add_last_date(self, burst_date):
        with open(self.path2processed_objects, 'r') as f:
            processed = json.load(f)
        processed['last_date_list'].append(burst_date)
        with open(self.path2processed_objects, 'w') as f:
            json.dump(processed, f)

    def read_astro_objects(self):
        with open(self.path2processed_objects, 'r') as objects:
            processed = json.load(objects)
        return processed

    def purge_astrodata(self, path2events):
        """Just clear all previous astro-data files except the freshest one"""
        try:
            actual = ''.join(path2events.split('/')[-1:])
            for f in os.listdir(self.path2astro_data):
                if f == actual:
                    pass
                else:
                    os.remove("{0}/{1}".format(self.path2astro_data, f))
        except:
            pass


    def insert_verse_in_dir(self, path2save, reply, sequence, burst_name, burst_date):
        with open('{0}/{1}:{2}:{3}:{4}::'.format(path2save, 'verse_by_xenosemantica', sequence, burst_name, burst_date), 'w') as f:
            f.write(reply)

    def make_nft_archive(self,
                         reply,
                         path2insight,
                         path2save,
                         event_id,
                         burst_name,
                         burst_date,
                         insights_hashes,
                         verse_hash):
        """
        - makes list of generated filenames for info_file,
        - Makes newdir for storing zip,
        - zip basename == dir name,
        - makes info file,
        - makes pdf,
        - Makes a zip file in data_run,
        - moves zip file to a newdir,
        - Calculates hash of the resulting zip
        - Adds hashsum to resulting dict
        - stores dir/files in ipfs storage
        - stores ipfs cid
        - makes a BC timestamp with Opentimestamp api
        """

        newdir = '{0}:{1}:{2}:{3}'.format(event_id, burst_name, burst_date, 'hashed_archive')
        path2newdir = os.path.join(path2save, newdir)
        path2ipfs_dir = 'xenosemantica_demo_files'

        archive_name = '{0}.zip'.format(newdir)
        path2archive = '{0}/{1}'.format(path2newdir, archive_name)
        temp_path2archive = '{0}/{1}'.format(self.path2data_run, archive_name)
        hash_zip_filename = '{0}.zip.hash'.format(newdir)
        img_verse_hashes_filename = '{0}.sha256'.format(newdir)

        with open('{0}/{1}'.format(path2save, img_verse_hashes_filename), 'w') as f:
            f.write('Xenosemantica images:\n{0}\n{1}\n{2}\n\nXenosemantica verse:\n{3}'.format(insights_hashes[0],
                                                                                               insights_hashes[1],
                                                                                               insights_hashes[2],
                                                                                               verse_hash))
        hash_img4_pdf = self.__make_hashes__(path2insight)

        demo_str = 'This is a demo pdf combined in the course of a single generative act of Xenosemantica.'
        demo_str2 = 'The original images and text are assembled in the corresponding files inside this archive.'
        make_pdf.makepdf_P(target ='Image',
                           reply = reply,
                           path2img = path2insight,
                           path2dir = path2save,
                           burst_name = burst_name,
                           burst_date = burst_date,
                           demo_string=demo_str,
                           demo_string2=demo_str2,
                           img_hash=hash_img4_pdf)

        make_pdf.makepdf_L(target ='Image',
                           reply = reply,
                           path2img = path2insight,
                           path2dir = path2save,
                           burst_name = burst_name,
                           burst_date = burst_date,
                           demo_string=demo_str,
                           demo_string2=demo_str2,
                           img_hash=hash_img4_pdf)

        with open('{0}/{1}.info'.format(path2save, archive_name), 'w') as info:
            info.write(' ')

        file_names = '\n'.join(map(str, os.listdir(path2save)))

        path2info = self.__make_infofile__(path2save,
                                           file_names,
                                           archive_name,
                                           burst_name,
                                           event_id,
                                           burst_date)

        with ZipFile(temp_path2archive, 'w') as zipObj:
            filenames_list = []
            for folderName, subfolders, filenames in os.walk('{0}'.format(path2save)):
                for filename in filenames:
                    # create complete filjepath of file in directory
                    filePath = os.path.join(folderName, filename)
                    # Add file to zip
                    zipObj.write(filePath, basename(filePath))
                    # add file to filenames list for info file
                    filenames_list.append(filename)

        os.mkdir(path2newdir)

        shutil.move(temp_path2archive, path2archive)

        archive_sha256 = self.__make_hashes__(path2archive)

        with open('{0}/{1}'.format(path2newdir, hash_zip_filename), 'w') as f:
            f.write('File:{0}\nSHA256:{1}'.format(archive_name, archive_sha256))

        dir4_ipfs_pdf_demo = '{0}/{1}'.format(path2ipfs_dir, burst_date)
        os.mkdir(dir4_ipfs_pdf_demo)

        demo_str = 'This is a demo pdf combined in the course of a single generative act of Xenosemantica.'
        demo_str2 = 'The original output files are assembled in the corresponding NFT archive.'
        path2_pdf =  make_pdf.makepdf_P(target='Demo',
                                        reply=reply,
                                        path2img=path2insight,
                                        path2dir=path2newdir,
                                        burst_name=burst_name,
                                        burst_date=burst_date,
                                        _hash=archive_sha256,
                                        demo_string = demo_str,
                                        demo_string2=demo_str2)

        shutil.copy(path2_pdf, dir4_ipfs_pdf_demo)
        shutil.copy(path2info, dir4_ipfs_pdf_demo)

        # pinata.pin_file_or_folder(dir4_ipfs_pdf_demo)

        result_subproc = subprocess.run(["node", "nft_store.mjs", dir4_ipfs_pdf_demo], stdout=subprocess.PIPE)
        demo_ipfs_hash = result_subproc.stdout.decode()[:-1]

        self.add_attributes_2astroobject(event_id,
                                         archive_name = archive_name,
                                         archive_sha256 = archive_sha256,
                                         hash4pdf = hash_img4_pdf,
                                         demo_ipfs_cid = demo_ipfs_hash,
                                         )
        return path2archive

    def make_bc_timestamp(self, path2file):
        subprocess.call(["ots", "-q", "stamp", path2file], stderr=subprocess.DEVNULL, stdout=subprocess.DEVNULL)

    def __make_infofile__(self,
                          path2save,
                          file_names,
                          archive_name,
                          burst_name,
                          event_id,
                          burst_date):

        with open('info_sample', 'r') as sample:
            sample = sample.read()

        sample = sample.format(archive_name = archive_name,
                          file_names = file_names,
                          sequence_name = event_id,
                          event_name = burst_name,
                          date_name = burst_date)

        with open('{0}/{1}.info'.format(path2save, archive_name), 'w') as info:
            info.write(sample)

        return '{0}/{1}.info'.format(path2save, archive_name)

    def __make_hashes__(self, path2file):
        BLOCKSIZE = 65536
        hasher = hashlib.sha256()
        with open(path2file, 'rb') as afile:
            buf = afile.read(BLOCKSIZE)
            while len(buf) > 0:
                hasher.update(buf)
                buf = afile.read(BLOCKSIZE)
        hash_sha256 = hasher.hexdigest()

        return hash_sha256

    def __make_str_hash__(self, verse):
        sha_signature = \
            hashlib.sha256(verse.encode()).hexdigest()
        return sha_signature

    def store_coords(self, processed_sequence, path2eventsfile):
        obj_coords = {}
        events = Table.read(path2eventsfile, hdu=1)
        obj_coords['RA_OBJ'] = events.meta.get('RA_OBJ')
        obj_coords['DEC_OBJ'] = events.meta.get('DEC_OBJ')
        self.add_attributes_2astroobject(processed_sequence, coords = obj_coords)

    def update_json4datatable(self, date, name, verse, hash):
        #  Append new entry to json needed for website chart
        with open(self.path2json2datable, 'r') as f:
            all_entries = json.load(f)
        new_entry = {}
        new_entry['date'] = date
        new_entry['name'] = name
        new_entry['verse'] = verse
        new_entry['img'] = hash
        all_entries.append(new_entry)
        with open(self.path2json2datable, 'w') as f:
            json.dump(all_entries)

