import random, math
from PIL import Image
from multiprocessing import Pool
from data_run import DataRun
import traceback

datarun = DataRun()

random.seed()

def well(x):
    '''A function which looks a bit like a well.'''
    return 1 - 2 / (1 + x * x) ** 8


def tent(x):
    '''A function that looks a bit like a tent.'''
    return 1 - 2 * abs(x)


class X:
    def eval(self, x, y):
        return x

    def __str__(self):
        return "x"


class Y:
    def eval(self, x, y):
        return y

    def __str__(self):
        return "y"


class Sin:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)
        self.phase = random.uniform(0, math.pi)
        self.freq = random.uniform(1.0, 6.0)

    def __str__(self):
        return "sin(pi*" + str(self.phase) + "+" + str(self.freq) + "*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.sin(math.pi * self.phase + self.freq * self.arg.eval(x, y))


class SinPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "sin(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.sin(math.pi * 2 * self.arg.eval(x, y))


class CosPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "cos(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.cos(math.pi * math.acos(y) * self.arg.eval(x, y))


#
# class aCosPi:
#     def __init__(self, prob):
#         self.arg = buildExpr(prob * prob)
#
#     def __str__(self):
#         return "acos(pi*" + str(self.arg) + ")"
#
#     def eval(self, x, y):
#         return math.acos(math.pi * 2 * self.arg.eval(x, y))

class Constant:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)
        self.c = (random.uniform(0, 1))

    def __str__(self):
        return "Constant(" + str(self.c) + ")"

    def eval(self, x, y):
        return self.c


# class Sum():
#     def __init__(self, prob):
#         self.arg1 = buildExpr(prob * prob)
#         self.arg2 = buildExpr(prob * prob)
#
#     def __str__(self):
#         return "Sum(" + str(self.arg1) + str(self.arg2) + ")"
#
#     def eval(self, x, y):
#         return np.average(self.arg1.eval(x, y), self.arg2.eval(x, y))


class Product:
    def __init__(self, prob):
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Product(" + str(self.arg1) + "," + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.arg1.eval(x, y)
        r2 = self.arg2.eval(x, y)
        r3 = r1 * r2
        return r3


class Mod():
    def __init__(self, prob):
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Mod(" + str(self.arg1) + "," + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.arg1.eval(x, y)
        r2 = self.arg2.eval(x, y)
        try:
            r3 = r1 % r2
            return r3
        except:
            return 0


class Level:
    def __init__(self, prob):
        self.treshold = random.uniform(-1.0, 1.0)
        self.level = buildExpr(prob * prob)
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Level(" + str(self.treshold) + str(self.level) + str(self.arg1) + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.level.eval(x, y)
        r2 = self.arg1.eval(x, y)
        r3 = self.arg2.eval(x, y)
        r4 = r2 if r1 < self.treshold else r3
        return r4


# class Mix():
#     def __init__(self, prob):
#         self.w = buildExpr(prob * prob)
#         self.arg1 = buildExpr(prob * prob)
#         self.arg2 = buildExpr(prob * prob)
#     def __repr__(self):
#         return "Mix(" + str(self.w) + str(self.arg1) + str(self.arg2) + ")"
#     def eval(self,x,y):
#         w = 0.5 * (self.w.eval(x,y)[0] + 1.0)
#         c1 = self.arg1.eval(x,y)
#         c2 = self.arg2.eval(x,y)
#         return np.average(c1,c2,)


class ArcSinPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "arcsin(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.asin(math.sin(math.pi * self.arg.eval(x, y)))


class TanPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "tan(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.tan(math.pi * math.acos(x) * self.arg.eval(x, y))


class ArcTang:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "atan(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.atan(math.pi * self.arg.eval(x, y))


class TanH:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "atanh(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.tanh(math.pi * self.arg.eval(x, y))


class Divide:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return str(self.arg) + "/ 0.03"

    def eval(self, x, y):
        return self.arg.eval(x, y) / 0.03


class Times:
    def __init__(self, prob):
        self.lhs = buildExpr(prob * prob)
        self.rhs = buildExpr(prob * prob)

    def __str__(self):
        return str(self.lhs) + "*" + str(self.rhs)

    def eval(self, x, y):
        return (self.lhs.eval(x, y)) * (self.rhs.eval(x, y))


class Turbo:
    def __init__(self, prob):
        self.factor1 = buildExpr(prob * prob)
        self.factor2 = buildExpr(prob * prob)
        self.factor3 = buildExpr(prob * prob)

    def __str__(self):
        return "Turbo(" + "(" + str(self.factor1) + str(self.factor2) + ")" + "*" + str(self.factor3) + ")"

    def eval(self, x, y):
        return (self.factor1.eval(x, y) + self.factor2.eval(x, y)) * self.factor3.eval(x, y)


class Well:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "Well(" + str(self.arg) + ")"

    def eval(self, x, y):
        return (well(self.arg.eval(x, y)))


class Tent:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "Tent(" + str(self.arg) + ")"

    def eval(self, x, y):
        return tent(self.arg.eval(x, y))


operand_0 = [CosPi, TanPi, Well, Divide, Tent, Turbo]  # [Mod, CosPi, SinPi, ArcSinPi, Times, Times, Divide, TanPi, Tent, Product, Well, Divide, Level, Divide, Times, TanH, Constant]
# [CosPi, Turbo, Divide, Tent], [CosPi, Turbo, Divide, Tent, Mod], [CosPi, Turbo, TanPi, Divide, Tent, Mod]
operand_1 = [Tent]


# operand_2 = [Turbo, Mod, CosPi, Times]


def buildExpr(prob=0.99):
    if random.random() < prob:
        return random.choice(operand_0)(prob)
    else:
        return random.choice([X, Y])()


def plotIntensity(exp):

    # print(exp)

    # with open('data_run/membrane_voltages_pckl', 'rb') as volts:
    #     mem_voltages = pickle.load(volts)

    mem_voltages = datarun.read_memvoltages() # returns dict

    mem_voltages = mem_voltages['m_voltages']

    canvasWidth = len(mem_voltages[0])
    canvas = Image.new("L", (canvasWidth, canvasWidth))

    for py in range(canvasWidth):

        count = random.randint(1, 100)

        if count <= 2:
            prob = 0.99
            if random.random() < prob:
                exp = random.choice(operand_1)(prob)
            else:
                exp = random.choice([X, Y])()

        for px in range(canvasWidth):
            x = float(mem_voltages[1][px])
            y = float(mem_voltages[0][py])
            z = exp.eval(x, y)

            # Scale [-1,1] result to [0,255].
            intensity = int(z * 127.5 + 127.5)
            canvas.putpixel((px, py), intensity)

    return canvas

# with open('{}/test_data/expressions_pckl'.format(os.getcwd()), 'rb') as exps:
# expressions = pickle.load(exps)

def makeImage(path2save, event_id):

    expressions = []
    for i in range(9):
        expressions.append(buildExpr())

    try:
        for k in range(1):

            pool = Pool(8)

            planes = pool.map(plotIntensity, [exp for exp in expressions])

            pool.close()

            # start_time = time.monotonic()

            image1 = Image.merge("RGB", (planes[0], planes[1], planes[2]))
            image2 = Image.merge("RGB", (planes[3], planes[4], planes[5]))
            image3 = Image.merge("RGB", (planes[6], planes[7], planes[8]))


            if k == 0:

                image1.save("{}{}.{}".format(path2save, event_id + '___1', 'png'), "PNG")
                image2.save("{}{}.{}".format(path2save, event_id + '___2', 'png'), "PNG")
                image3.save("{}{}.{}".format(path2save, event_id + '___3', 'png'), "PNG")

            if k == 1:

                image1.save("{}{}.{}".format(path2save, event_id + '___4', 'png'), "PNG")
                image2.save("{}{}.{}".format(path2save, event_id + '___5', 'png'), "PNG")
                image3.save("{}{}.{}".format(path2save, event_id + '___6', 'png'), "PNG")

        return True

    except Exception:
        traceback.print_exc()
        return False

# makeImage() # testing
