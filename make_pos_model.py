import markovify_xs
import spacy
import json

nlp = spacy.load("en_core_web_lg")

class POSifiedText(markovify_xs.Text):
    def word_split(self, sentence):
        return ["::".join((word.orth_, word.pos_)) for word in nlp(sentence)]

    def word_join(self, words):
        sentence = " ".join(word.split("::")[0] for word in words)
        return sentence


# Builds model with NLTK
#
# import nltk
#
# class POSifiedText(markovify_xs.Text):
#     def word_split(self, sentence):
#         words = re.split(self.word_split_pattern, sentence)
#         words = [ "::".join(tag) for tag in nltk.pos_tag(words) ]
#         return words
#
#     def word_join(self, words):
#         sentence = " ".join(word.split("::")[0] for word in words)
#         return sentence


with open("markov_models/roberts.txt") as f:
    text = f.read()

text_model = POSifiedText(text)

model_json = text_model.to_json()

with open('markov_models/roberts_pos3_model.json', 'w') as json_file:  # change file name if NLTK is used
  json.dump(model_json, json_file)