import random, math
from PIL import Image
import json
import os

workdir = os.getcwd()
random.seed()

def well(x):
    '''A function which looks a bit like a well.'''
    return 1 - 2 / (1 + x * x) ** 8


def tent(x):
    '''A function that looks a bit like a tent.'''
    return 1 - 2 * abs(x)


class X:
    def eval(self, x, y):
        return x

    def __str__(self):
        return "x"


class Y:
    def eval(self, x, y):
        return y

    def __str__(self):
        return "y"


class Sin:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)
        self.phase = random.uniform(0, math.pi)
        self.freq = random.uniform(1.0, 6.0)

    def __str__(self):
        return "sin(pi*" + str(self.phase) + "+" + str(self.freq) + "*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.sin(math.pi * self.phase + self.freq * self.arg.eval(x, y))


class SinPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "sin(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.sin(math.pi * 2 * self.arg.eval(x, y))


class CosPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "cos(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.cos(math.pi * math.acos(y) * self.arg.eval(x, y))


# class aCosPi:
#     def __init__(self, prob):
#         self.arg = buildExpr(prob * prob)
#
#     def __str__(self):
#         return "acos(pi*" + str(self.arg) + ")"
#
#     def eval(self, x, y):
#         return math.acos(math.pi* self.arg.eval(x, y))

class Constant:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)
        self.c = (random.uniform(0, 1))

    def __str__(self):
        return "Constant(" + str(self.c) + ")"

    def eval(self, x, y):
        return self.c

#
# class Sum():
#     def __init__(self, prob):
#         self.arg1 = buildExpr(prob * prob)
#         self.arg2 = buildExpr(prob * prob)
#
#     def __str__(self):
#         return "Sum(" + str(self.arg1) + str(self.arg2) + ")"
#
#     def eval(self, x, y):
#         return np.average(self.arg1.eval(x, y), self.arg2.eval(x, y))


class Product:
    def __init__(self, prob):
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Product(" + str(self.arg1) + "," + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.arg1.eval(x, y)
        r2 = self.arg2.eval(x, y)
        r3 = r1 * r2
        return r3


class Mod():
    def __init__(self, prob):
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Mod(" + str(self.arg1) + "," + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.arg1.eval(x, y)
        r2 = self.arg2.eval(x, y)
        try:
            r3 = r1 % r2
            return r3
        except:
            return 0


class Level:
    def __init__(self, prob):
        self.treshold = random.uniform(-1.0, 1.0)
        self.level = buildExpr(prob * prob)
        self.arg1 = buildExpr(prob * prob)
        self.arg2 = buildExpr(prob * prob)

    def __str__(self):
        return "Level(" + str(self.treshold) + str(self.level) + str(self.arg1) + str(self.arg2) + ")"

    def eval(self, x, y):
        r1 = self.level.eval(x, y)
        r2 = self.arg1.eval(x, y)
        r3 = self.arg2.eval(x, y)
        r4 = r2 if r1 < self.treshold else r3
        return r4


# class Mix():
#     def __init__(self, prob):
#         self.w = buildExpr(prob * prob)
#         self.arg1 = buildExpr(prob * prob)
#         self.arg2 = buildExpr(prob * prob)
#     def __repr__(self):
#         return "Mix(" + str(self.w) + str(self.arg1) + str(self.arg2) + ")"
#     def eval(self,x,y):
#         w = 0.5 * (self.w.eval(x,y)[0] + 1.0)
#         c1 = self.arg1.eval(x,y)
#         c2 = self.arg2.eval(x,y)
#         return np.average(c1,c2,)


class ArcSinPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "arcsin(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.asin(math.sin(math.pi * self.arg.eval(x, y)))


class TanPi:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "tan(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.tan(math.pi * math.acos(x) * self.arg.eval(x, y))


class ArcTang:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "atan(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.atan(math.pi * self.arg.eval(x, y))


class TanH:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "atanh(pi*" + str(self.arg) + ")"

    def eval(self, x, y):
        return math.tanh(math.pi * self.arg.eval(x, y))


class Divide:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return str(self.arg) + "/ 0.03"

    def eval(self, x, y):
        return self.arg.eval(x, y) / 0.03


class Times:
    def __init__(self, prob):
        self.lhs = buildExpr(prob * prob)
        self.rhs = buildExpr(prob * prob)

    def __str__(self):
        return str(self.lhs) + "*" + str(self.rhs)

    def eval(self, x, y):
        return (self.lhs.eval(x, y)) * (self.rhs.eval(x, y))


class Turbo:
    def __init__(self, prob):
        self.factor1 = buildExpr(prob * prob)
        self.factor2 = buildExpr(prob * prob)
        self.factor3 = buildExpr(prob * prob)

    def __str__(self):
        return "Turbo(" + "(" + str(self.factor1) + str(self.factor2) + ")" + "*" + str(self.factor3) + ")"

    def eval(self, x, y):
        return (self.factor1.eval(x, y) + self.factor2.eval(x, y)) * self.factor3.eval(x, y)



class Well:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "Well(" + str(self.arg) + ")"

    def eval(self, x, y):
        return (well(self.arg.eval(x, y)))


class Tent:
    def __init__(self, prob):
        self.arg = buildExpr(prob * prob)

    def __str__(self):
        return "Tent(" + str(self.arg) + ")"

    def eval(self, x, y):
        return tent(self.arg.eval(x, y))


def buildExpr(prob=0.99):
    if random.random() < prob:
        return random.choice(
            [[CosPi, Turbo, TanPi, Divide, Tent, Mod]])(
            prob)
    else:
        return random.choice([X, Y])()


def plotIntensity(exp, mem_voltages):

    pixelsPerUnit = len(mem_voltages[0])
    canvasWidth = pixelsPerUnit
    canvas = Image.new("L", (canvasWidth, canvasWidth))

    for py in range(canvasWidth):
        count = random.randint(1, 100)

        if count < 2:
            exp = buildExpr(prob=0.99)

        for px in range(canvasWidth):
            x = float(mem_voltages[0][py]) # py/px
            y = float(mem_voltages[0][px]) # py/px
            z = exp.eval(x, y)

            # Scale [-1,1] result to [0,255].
            intensity = int(z * 127.5 + 127.5)
            canvas.putpixel((px, py), intensity)

    return canvas


def plotColor(redExp, greenExp, blueExp, mem_voltages):
    # pixelsPerUnit = len(mem_voltages[0])
    redPlane = plotIntensity(redExp,mem_voltages)
    print('red=', redExp)
    greenPlane = plotIntensity(greenExp,mem_voltages)
    print('green=', greenExp)
    bluePlane = plotIntensity(blueExp, mem_voltages)
    print('blue=', blueExp)
    return Image.merge("RGB", (redPlane, greenPlane, bluePlane))


def makeImage(mem_voltages, numPics=3):

    with open('{}/data_run/grbs_processed.json'.format(os.getcwd()), 'r') as grbs:
        grb_list = json.load(grbs)  # GRB's name defines the directory/file name of an insight
    os.chdir('{}/insights_img/'.format(workdir))
    os.mkdir('{}'.format(''.join(grb_list[-1:])))
    insight_dir = ''.join(grb_list[-1:])
    os.chdir('{}/insights_img/{}'.format(workdir, insight_dir))
    for i in range(numPics):
        try:
            redExp = buildExpr()
            greenExp = buildExpr()
            blueExp = buildExpr()

            image = plotColor(redExp, greenExp, blueExp, mem_voltages)

            image.save("{}-{}.{}".format(''.join(grb_list[-1:]),str(i),'png'),"PNG")

        except:
            os.chdir(workdir)
            print('no insight')
            pass

    os.chdir(workdir)

    return '{}/insights_img/{}/'.format(os.getcwd(),''.join(grb_list[-1:]))
