import markovify_xs
import json

tries = 30
max_chars = 300

path2model = 'running_m_model/combined_mod3.json'  # use posified (better?) or naive models

with open(path2model) as f:
    markov_json_model = json.load(f)

# testing predicts:
# predicts = ['chair', 'cellular', 'confectionary', 'mouth', 'digital', 'balusters', 'bench', 'Biro', 'pen', 'car',
#             'wallet', 'reaper', 'packet', 'water', 'filing', 'swimming', 'cap', 'microcomputer', 'chainlink',
#             'folding', 'prayer', 'theater', 'scoreboard', 'rocker', 'box', 'umbrella', 'comforter', 'case',
#             'crib', 'paddlewheel', 'phone', 'ring-binder', 'theatre', 'cathode-ray', 'woolen', 'hanky',
#             'throne', 'plow', 'clock', 'cover', 'palace', 'claw', 'picture', 'window', 'jacket', 'pin',
#             'monitor', 'CRT', 'bottle', 'pole', 'one-armed', 'web', 'bannister', 'fountain', 'hankey',
#             'fence', 'rug', 'computer', 'site', 'notecase', 'ruler', 'billfold', 'freight', 'laptop', 'eraser',
#             'mat', 'comfort', 'wrapper', 'harmonica', 'welcome', 'bookcase', 'carton', 'slot', 'handkerchief',
#             'bandit', 'woollen', 'shower', 'balustrade', 'cabinet', 'grand', 'watch', 'harvester', 'pillow',
#             'cellphone', 'accordion', 'paintbrush', 'television', 'file', 'whistle', 'house', 'movie', 'wheel',
#             'book', 'cart', 'desktop', 'doormat', 'rubber', 'truck', 'picket', 'wool', 'cinema', 'rocking',
#             'letter', 'menu', 'screen', 'candy', 'squeeze', 'internet', 'tray', 'hankie', 'pocketbook',
#             'shade', 'banister', 'shopping', 'website', 'rule', 'ballpoint', 'library', 'cot', 'oscilloscope',
#             'drumstick', 'telephone', 'quilt', 'handrail', 'curtain', 'hand-held', 'paddle', 'stage', 'park',
#             'mobile', 'scope', 'dust', 'bow', 'analog', 'bluetick', 'paling', 'piano', 'system', 'fire',
#             'engine', 'CRO', 'confectionery', 'binder', 'pencil', 'envelope', 'puff', 'mailbox', 'safety',
#             'organ', 'bathing', 'ballpen', 'pinwheel', 'hook', 'cell', 'comic', 'plough', 'harp', 'basket', 'store']

predicts = []

# add some new methods and attributes to markovify_xs.Text class
class NewMakeShort(markovify_xs.Text):

    predicts = predicts

    def make_short_with_start(self, predicts, start_word, max_chars, min_chars=0):

        for _ in range(tries):
            sentence = self.make_sentence_with_start(predicts, start_word, strict=False)
            if sentence and len(sentence) <= max_chars and len(sentence) >= min_chars:
                return sentence


reconstituted_model = NewMakeShort.from_json(markov_json_model)

# reconstituted_model.compile()

def generate_verses(predicts, reconstituted_model=reconstituted_model):
    '''
    takes: Markov reconstituted_model from a json file and a list of 'predicts',
    tries to generate a sequence, using Markov model and a list of 'predicts' as start words
    returns: a list [0] of normalized verses of a desired verse_length each
    a list 1 of predicts that triggered generating process
    and a list 2 of predicts that failed producing verses
    '''
    verses = []
    trigger_predicts = []  # the predicts which triggered the sentence generation
    zero_predicts = []  # the predicts with zero output

    for i in predicts:
        generate = ''
        try:
            generate = reconstituted_model.make_short_with_start(predicts, i + '::NOUN', max_chars=max_chars, min_chars=0)
            # generate = reconstituted_model.make_short_sentence(predicts, max_chars=max_chars, min_chars=0)
            # now removing tokens, extra spaces etc to normalize a generated sequence, add expressions if needed
            for r in (
                    ('::NOUN', ''), ('::VERB', ''), ('::PRON', ''), ('::ADP', ''), ('\n', ''), ('::PROPN', ''),
                    ('::DET', ''),
                    ('::PUNCT', ''), ('::SPACE', ''), ('::PROPN', ''), ('::ADJ', ''), ('::NUM', ''), ('::SYM', ''),
                    ('::ADV', ''), ('::CONJ', ''),
                    ('::CCONJ', ''), ('::AUX', ''), ('::PART', ''), ('::INTJ', ''), ('::SCONJ', ''), ('::X', ''),
                    (' ,', ','), (' ?', '?'),
                    (' .', '.'), (' ;', ''), (' !', ''), ('-', ''), (" '", "'"), ('  ', ' '), ('"', '')):
                generate = generate.replace(*r)
            verses.append(generate)
            trigger_predicts.append(i)

            # an alternative way to normalize a generated sequence, but less efficient, i.e. more regex needed:
            # gen_spl = re.split('::|PRON|PROPN|ADJ|VERB|ADP|NOUN|PUNCT|NUM|SYM|SPACE|DET|ADV|CONJ|CCONJ|AUX|PART|INTJ|SCONJ|X|\n|',
            #     generate)

        except:
            if generate:
                pass
            else:
                zero_predicts.append(i)

    return verses, trigger_predicts, zero_predicts


# testing predicts:

# print(generate_verses(predicts))
