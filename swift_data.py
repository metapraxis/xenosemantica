import bs4 as bs
import requests
import json
from six.moves import urllib
import time
from socket import error as SocketError


class SwiftData():

    quickdata_url = 'https://swift.gsfc.nasa.gov/sdc/ql/'
    file_ext = 'gz'

    def __init__(self, time2sleep):
        self.time2sleep = time2sleep
        # self.workdir = workdir # delete as workdir is a default environment var


    def __get_source_from_swift__(self):
        while True:
            try:
                source = requests.get(SwiftData.quickdata_url)
                break
            except requests.exceptions.RequestException:
                print('RequestException, sleep 60s')
                time.sleep(self.time2sleep)
        return source

    def __get_url_paths__(self, url_seq):

        response = requests.get(url_seq, params={})

        if response.ok:
            response_text = response.text
        else:
            print(response)
            return response.raise_for_status()  # handling an error if virtual url fails

        soup = bs.BeautifulSoup(response_text, 'html.parser')
        parent = [url_seq + node.get('href') for node in soup.find_all('a') if node.get('href').endswith(SwiftData.file_ext)]

        return parent

    def __get_fits_file__(self, path2largest, path2download):
        while True:
            try:
                print('getting fits file....')
                urllib.request.urlretrieve(path2largest, path2download)
                return True
            except SocketError:
                print('SocketError, sleep 60s')
                time.sleep(self.time2sleep)

    def __build_table__(self):

        quicklook_table = []

        source = self.__get_source_from_swift__()

        soup = bs.BeautifulSoup(source.content, 'lxml')
        table = soup.table
        table_rows = table.find_all('tr')

        for tr in table_rows:
            td = tr.find_all('td')
            row = [i.text for i in td]
            quicklook_table.append(row)

        return quicklook_table

    def __get_processed_grbs__(self):

        try:
            with open('data_run/grbs_processed.json', 'r') as grbs_processed:
                grbs_base = json.load(grbs_processed)  # returns a list
        except:  # if you start with an empty list of GRBs
            grbs_base = []

        return grbs_base


    def get_fits_list(self):

        '''
        Scans SWIFT's Mission QuickLook Data page for a new GRB entry
        If a new GRB pops up it tries to download the largest events file associated with it (IF there is any) and
        returns a local path to a new events file (where filename consists of: - observed time, - object's name, - sequence, - seq. version
        Returns False if no new GRB or no events file is found
        '''
        quicklook_table = self.__build_table__()

        grbs_base = self.__get_processed_grbs__()

        for i in quicklook_table[1:]:  # NB: 1st row in quicklook_table is empty!

            if any(x in i[2] for x in ['GRB', 'Burst']):  # "GRB" or 'Burst' mark desired sequence in a quicklook_table

                if '{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1]) in grbs_base:
                    pass
                else:
                    try:
                        seq_vers = str(i[0]) + '.' + str(i[1])  # composing a virtual sequence
                        print(seq_vers)

                        url_seq = 'https://swift.gsfc.nasa.gov/data/swift/sw{}/bat/event/'.format(seq_vers)  # composing a virtual path

                        files_paths = self.__get_url_paths__(url_seq)  # checking a virtual path (to handle an http error if failed)

                        files_length = {}

                        for k in files_paths:
                            response = requests.head(k)
                            files_length[k] = (int(response.headers['Content-Length']))

                        path2largest = max(files_length, key=lambda i: files_length[i])

                        path2download = 'astro_data/{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1])

                        if self.__get_fits_file__(path2largest, path2download):
                            pass
                        else:
                            break

                        grbs_base.append('{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1]))  # 3 - observed time, 2 - object, 0 - sequence, 1 - version

                        print('New GRB is ~/astro_data/{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1]))

                        with open('data_run/grbs_processed.json', 'w') as grbs_processed:
                            json.dump(grbs_base, grbs_processed)

                        print('astro_data/{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1]))

                        return 'astro_data/{}:{}:{}:{}'.format(i[3], i[2], i[0][:11], i[1])

                    except requests.exceptions.HTTPError:
                        pass

        return None
