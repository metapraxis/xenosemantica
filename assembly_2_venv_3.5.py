import multiprocMakeInsights
import class_nsights
import verse_from_predicts
import pickle
import time
import json
import os

workdir = os.getcwd()

while True:

    with open('{}/data_run/membrane_voltages_pckl'.format(os.getcwd()), 'rb') as m_volts:
        m_voltages = pickle.load(m_volts, encoding='latin1')
    with open('{}/data_run/grbs_processed.json'.format(os.getcwd()), 'r') as grbs:
        grbs_list = json.load(grbs)  # GRB's names (e.g. ["GRB Discovered Elsewhere:00020895",...]) define the keys of the output_dict
    with open('{}/data_run/output_dict.json'.format(os.getcwd()), 'r') as output:
        output_dict = json.load(output)

    if m_voltages['flag']:

        path2insights = multiprocMakeInsights.makeImage()

        if path2insights:

            m_voltages['flag'] = False

            with open('{}/data_run/membrane_voltages_pckl'.format(os.getcwd()), 'wb') as m_volts:
                pickle.dump(m_voltages, m_volts)

            event = ''.join(grbs_list[-1:])

            paths = []
            for f in os.listdir(path2insights):
                paths.append(path2insights + f)

            get_predicts = list(class_nsights.get_predicts(paths))

            verses = verse_from_predicts.generate_verses(get_predicts)

            output_dict[event] = verses
            with open('{}/data_run/output_dict.json'.format(os.getcwd()), 'w') as output:
                json.dump(output_dict, output)

            print('{} : {}'.format(event, verses))

        else:
            print('no insights')
            pass

    else:
        print('flag is False, waiting 60 mins')
        time.sleep(3600)

